/*
* 自定义的Ajax请求封装
* */
;(function (global) {
    var Ajax = function (options) {
        var ajax;
        // 请求地址
        this.url = null;
        // 参数
        this.data = null;
        // 请求方式
        this.method = "GET";
        // 返回数据格式化方式 json xml text
        this.dataType = "json";
        // false 同步请求，true异步请求（默认）
        this.async = true;
        // 返回的原数据
        this.resultText = "";
        // 返回结果
        this.result = null;
        // 匿名处理函数
        this.fun = null;
        // 执行状态【是否执行成功】
        this.execStatus = false;
        // 错误信息
        this.errorMsg = null;
        // 错误代码
        this.code = null;
        // 执行ajax类型
        this.type = "jquery";
        // 组装参数
        this.parseParam = function (param, key) {
            var paramStr = "";
            if (param instanceof String || param instanceof Number || param instanceof Boolean) {
                paramStr += "&" + key + "=" + encodeURIComponent(param);
            } else {
                $.each(param, function (i) {
                    var k = key == null ? i : key + (param instanceof Array ? "[" + i + "]" : "." + i);
                    paramStr += '&' + ajax.parseParam(this, k);
                });
            }
            return paramStr.substr(1);
        };
        this.jsonParse = function () { // 字符串转json对象
            if (ajax.resultText.indexOf("{") >= 0) {
                ajax.result = JSON.parse(ajax.resultText);
            } else {
                ajax.result = ajax.resultText;
            }
        };
        this.xmlParse = function () { // 字符串转xml对象
            if (ajax.resultText.indexOf("<") >= 0) {
                try {
                    if (document.all) {
                        var xmlDom = new ActiveXObject("Microsoft.XMLDOM");
                        xmlDom.loadXML(ajax.resultText);
                    } else {
                        var xmlDom = new DOMParser().parseFromString(ajax.resultText, "text/xml");
                        ajax.result = xmlDom;
                    }
                } catch (e) {
                    throw new Error("xml实例化失败");
                    return;
                }
            } else {
                ajax.result = ajax.resultText;
            }

        };
        // 请求初始化
        this.init = function (u, p, m, r, a, f) {
            this.url = (u === undefined || u === "") ? this.url : u;
            this.data = (p === undefined || p === "") ? this.data : p;
            this.method = (m === undefined || m === "" || typeof m === "function") ? this.method : m;
            this.dataType = ((r === undefined || r === "" || typeof r === "function") ? this.dataType : r);
            this.async = (a === undefined || a === "" || typeof a === "function") ? this.async : a;
            this.fun = (f === undefined || f === "") ? this.fun : f;
            // 识别是否有匿名函数
            if (typeof p === "function") {
                this.fun = p;
            }
            if (typeof m === "function") {
                this.fun = m;
            }
            if (typeof r === "function") {
                this.fun = r;
            }
            if (typeof a === "function") {
                this.fun = a;
            }
            ajax = this;
            if (ajax.url === "" || ajax.url === undefined) {
                throw new Error("缺少请求地址");
            } else {
                if (ajax.type === "original" && typeof ajax.data === "object" && ajax.method == "post") {
                    ajax.data = ajax.parseParam(ajax.data);
                    if (ajax.url.indexOf("?") >= 0 ) {
                        // ajax.url += "&"+ajax.data;
                    } else {
                        ajax.url += "?" + ajax.data;
                    }
                }
            }

        };
        // 执行格式化数据函数 并返回
        this.resultOption = function (r) {

            // jquery 不用在执行格式化了
            if (ajax.type !== "jquery") {
                eval("ajax." + ajax.dataType + "Parse()");
            }
            if (r !== undefined && r !== "") {
                ajax.result = r;
            }
            // 调用匿名函数
            if (typeof ajax.fun === "function") {
                ajax.fun(ajax.result);
            }
        };
        // 原生的Ajax请求
        this.ajaxOriginal = function (u, p, m, r, a, f) {
            this.type = "original";
            this.init(u, p, m, r, a, f);
            this.execStatus = false;
            // 第一步： 获得XMLHttpRequest对象
            var xmlhttp;
            try {
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
            } catch (e) {
                throw new Error("原生Ajax实例化失败");
            }

            // 异步请求先监听
            if (ajax.async) {
                // 第二步： 设置状态监听函数
                xmlhttp.onreadystatechange = function () {
                    /*
                     状态 xmlhttp.readyState :
                     0: 请求未初始化
                     1: 服务器连接已建立
                     2: 请求已接收
                     3: 请求处理中
                     4: 请求已完成，且响应已就绪
                     状态码 xmlhttp.status
                     返回的文本信息 xmlhttp.responseText
                     */
                    // 第五步：在监听函数中，判断readyState=4 && status=200表示请求成功
                    ajax.code = xmlhttp.status;         // http响应状态码
                    ajax.errorMsg = xmlhttp.statusText; // 执行返回的消息
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        // 第六步： 使用responseText、responseXML接受响应数据，并使用原生JS操作DOM进行显示
                        ajax.resultText = xmlhttp.responseText;
                        ajax.execStatus = true;
                        ajax.resultOption();
                    }
                };
            }

            if (ajax.method === "POST" || ajax.method === "post") {
                xmlhttp.open("POST", ajax.url, ajax.async);//true异步请求，false同步
                // 如果是post请求
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            } else {
                // 第三步： open一个链接
                xmlhttp.open("GET", ajax.url, ajax.async);//true异步请求，false同步
            }

            // 第四步： send一个请求。 可以发送对象和字符串，不需要传递数据发送null
            xmlhttp.send(this.data);

            // 同步请求后监听
            if (ajax.async === false) {
                ajax.code = xmlhttp.status;         // http响应状态码
                ajax.errorMsg = xmlhttp.statusText; // 执行返回的消息
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        ajax.resultText = xmlhttp.responseText;
                        ajax.execStatus = true;
                        ajax.resultOption();
                    }
                };
            }
        };
        // JQueryAjax请求
        this.ajaxQuery = function (u, p, m, r, a, f) {
            this.type = "jquery";
            this.init(u, p, m, r, a, f);
            $.ajax({
                url: ajax.url,
                async: ajax.async,
                type: ajax.method,
                data: ajax.data,
                dataType: ajax.dataType,  // 这里会处理返回信息格式
                success: function (result, status, xhr) {
                    ajax.execStatus = true;
                    ajax.errorMsg = xhr.statusText;
                    ajax.code = xhr.status;
                    ajax.resultText = result;
                    ajax.resultOption(result);
                },
                error: function (xhr, status, error) {
                    ajax.execStatus = false;
                    ajax.errorMsg = xhr.statusText;
                    ajax.code = xhr.status;
                }
            });
        };

        // 初始化
        if (options === undefined) {
            this.init();
        } else {
            this.init(
                options.url,
                options.data,
                options.method,
                options.dataType,
                options.async,
                options.callback
            );
        }
    };
    window.Ajax = Ajax;
})(this);

/*
 简易使用封装
 $$.request({
       url:"ajaxdemo.php",
       callback:function (r) {
           console.log(r);
       }
   });
* */
var $$ = {
    request: function (obj, type) {
        var aj;
        try {
            // aj = new Ajax(obj.url, obj.data, obj.method, obj.dataType, obj.async, obj.callback);
            aj = new Ajax(obj);
        } catch (e) {
            throw new Error("Ajax对象不存在");
            return;
        }
        if (type == "original") {
            aj.ajaxOriginal();
        } else { // 默认使用jquery版ajax
            aj.ajaxQuery();
        }
    }
};


/*
* 帮助知识：
* console.log(arguments[0]);
* @arguments：可以查看当前函数或对象中（使用体里）的所有传参
*
* js文件中引入其他js文件
document.write("<script language=javascript src='auth/ajax_year.js' async='async'></script>");
* */
/**
 * @AJAX
 * 100——客户必须继续发出请求

 101——客户要求服务器根据请求转换HTTP协议版本

 200——交易成功

 201——提示知道新文件的URL

 202——接受和处理、但处理未完成

 203——返回信息不确定或不完整

 204——请求收到，但返回信息为空

 205——服务器完成了请求，用户代理必须复位当前已经浏览过的文件

 206——服务器已经完成了部分用户的GET请求

 300——请求的资源可在多处得到

 301——删除请求数据

 302——在其他地址发现了请求数据

 303——建议客户访问其他URL或访问方式

 304——客户端已经执行了GET，但文件未变化

 305——请求的资源必须从服务器指定的地址得到

 306——前一版本HTTP中使用的代码，现行版本中不再使用

 307——申明请求的资源临时性删除

 400——错误请求，如语法错误

 401——请求授权失败

 402——保留有效ChargeTo头响应

 403——请求不允许

 404——没有发现文件、查询或URl

 405——用户在Request-Line字段定义的方法不允许

 406——根据用户发送的Accept拖，请求资源不可访问

 407——类似401，用户必须首先在代理服务器上得到授权

 408——客户端没有在用户指定的时间内完成请求

 409——对当前资源状态，请求不能完成

 410——服务器上不再有此资源且无进一步的参考地址

 411——服务器拒绝用户定义的Content-Length属性请求

 412——一个或多个请求头字段在当前请求中错误

 413——请求的资源大于服务器允许的大小

 414——请求的资源URL长于服务器允许的长度

 415——请求资源不支持请求项目格式

 416——请求中包含Range请求头字段，在当前请求资源范围内没有range指示值，请求也不包含If-Range请求头字段

 417——服务器不满足请求Expect头字段指定的期望值，如果是代理服务器，可能是下一级服务器不能满足请求

 500——服务器产生内部错误

 501——服务器不支持请求的函数

 502——服务器暂时不可用，有时是为了防止发生系统过载

 503——服务器过载或暂停维修

 504——关口过载，服务器使用另一个关口或服务来响应用户，等待时间设定值较长

 505——服务器不支持或拒绝支请求头中指定的HTTP版本
 * */